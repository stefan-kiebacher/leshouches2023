// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
//#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/DirectFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"

#include "fastjet/JetDefinition.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/contrib/LundGenerator.hh"

#include <string.h>

/*

	 A RIVET routine for trying to understand what measurements would be useful for improving MC predictions
	 Currently making a di-jet selection on events based on a pT balance requirement, and focused on relatively central jets

	 Written by Jennifer Roloff <jroloff2@gmail.com>


	 List of current included observables:

 *Lund and Cambridge multiplicities
 Suggestion (and code) from Matt LeBlanc <matt.leblanc@cern.ch>
 The University of Manchester 2023
https://arxiv.org/abs/2212.05076
This routine adapts some functions from R. Medves (Oxford University), provided privately.

 *Hadron-correlations within jets
 Currently just the dR between particles within jets
 Suggestion from Simon

 *The Lund jet plane
 Code taken from Frederic Dreyer

 *dPsi for leading particles within jets
 Suggestion from Sylvia
 Similar to Figure 3b of https://arxiv.org/abs/2207.09467

 *Hadron multiplicities and energy fractions, with inspiration taken from
https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2022-021/

 * Energy-energy correlators
https://arxiv.org/abs/2203.07113


For more suggestions, please email Jennifer Roloff

*/

namespace Rivet {


	/// @brief MC_LESHOUCHES23 // placeholder number 1111111 for now
	class MC_LESHOUCHES23 : public Analysis {
		public:

			/// Constructor
			RIVET_DEFAULT_ANALYSIS_CTOR(MC_LESHOUCHES23);

			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Multiplicity code from Rok Medves!

			/// \brief Function that returns the Lund multiplicity of a clustered jet `jet'
			/// \param jet Pseudojet for which Lund mult is counted
			/// \param ktcut the ktcut value used for the counting
			///
			/// This function essentially reclusters the passed-in jet
			/// with Cambridge/Aachen and then counts it's Lund multiplicity
			/// with count_lund_multiplicity()
			double  getCorrelation(const PseudoJet& particle1, const PseudoJet& particle2){
				return particle1.delta_R(particle2);
			}

			// This part from F. Dreyer
			struct Declustering {
				// the (sub)jet, and its two declustering parts, for subsequent use
				fastjet::PseudoJet jj, j1, j2;
				// variables of the (sub)jet about to be declustered
				double pt, m;
				// properties of the declustering; NB kt is the _relative_ kt
				// = pt2 * delta_R (ordering is pt2 < pt1)
				double pt1, pt2, delta_R, z, kt, varphi;
			};


			// Code below from F. Dreyer
			// https://github.com/rappoccio/fastjet-tutorial/blob/master/lund-jet-example/lund.hh
			std::vector< Declustering>  jet_declusterings(const fastjet::PseudoJet & jet_in){
				fastjet::JetDefinition jd(fastjet::cambridge_algorithm, m_jetRadius);
				fastjet::Recluster rc(jd);
				fastjet::PseudoJet j = rc.result(jet_in);

				std::vector<Declustering > result;
				fastjet::PseudoJet jj, j1, j2;
				jj = j;

				while (jj.has_parents(j1,j2)) {
					Declustering declust;

					// make sure j1 is always harder branch 
					if (j1.pt2() < j2.pt2()) {
						std::cout  << j1.pt2() << "\t" << j2.pt2() << std::endl;
						fastjet::PseudoJet jTemp;
						jTemp = j1;
						j1 = j2;
						j2 = jTemp;
						std::cout  << j1.pt2() << "\t" << j2.pt2() <<    std::endl;
					}

					// store the subjets themselves
					declust.jj   = jj;
					declust.j1   = j1;
					declust.j2   = j2;

					// get info about the jet 
					declust.pt   = jj.pt();
					declust.m    = jj.m();

					// collect info about the declustering
					declust.pt1     = j1.pt();
					declust.pt2     = j2.pt();
					declust.delta_R = j1.delta_R(j2);
					declust.z       = declust.pt2 / (declust.pt1 + declust.pt2);
					declust.kt      = j2.pt() * declust.delta_R;

					// this is now phi along the jet axis, defined in a
					// long. boost. inv. way
					declust.varphi = atan2(j1.rap()-j2.rap(), j1.delta_phi_to(j2));

					// add it to our result
					result.push_back(declust);

					// follow harder branch
					jj = j1;
				}

				return result;
				if (result.size() == 0) {
					std::cerr << "HEY THERE, empty declustering. Jet p_t and n_const = " << jet_in.pt() << " " << jet_in.constituents().size() << std::endl;
				}
			}

			// Returns the deltaPsi between the leading two emissions in a jet
			// Returns -10 if there are fewer than 2 emissions
			double deltaPsi(std::vector< Declustering> clust){
				double pi = 3.14159;
				if(clust.size() < 2) return -10;
				double varPhi1 = clust[0].varphi;
				double varPhi2 = clust[1].varphi;
				double deltaVarPhi = varPhi1 - varPhi2;
				if(deltaVarPhi < 0) deltaVarPhi = 2*pi - deltaVarPhi;
				if(deltaVarPhi > 2*pi) deltaVarPhi = deltaVarPhi - 2*pi;
				return deltaVarPhi;

			}


			int lund_multiplicity(const PseudoJet& jet, const double& trackfrac = 1, const double& ktcut = 1){
				// Recluster the jet with C/A
				JetDefinition CA_jet_def = JetDefinition(fastjet::cambridge_algorithm, fastjet::JetDefinition::max_allowable_R);
				ClusterSequence ca_cs(jet.constituents(), CA_jet_def);

				// Simple assert to see that things have gone right:
				// expect only one jet to be found
				assert(ca_cs.inclusive_jets().size() == 1 && "Flawed event for dijet Lund multiplicity");    

				// Now we can count
				return count_lund_multiplicity(ca_cs.inclusive_jets()[0], trackfrac, ktcut);
			}

			/// \brief Function that resursively counts lund multiplicity
			/// \param jet Pseudojet for which Lund mult is counted
			/// \param ktcut the ktcut value used for the counting
			///
			/// The actual muscle of the function lund_multiplicity().
			///
			/// This function implements the Lund multiplicity counting
			/// as described in arXiv:2205.02861:
			/// 1.   set the N_Lund = 1
			/// 2.   undo one clustering step j -> j1 + j2 (where ptj1 > ptj2)
			/// 3.   determine kt = min(ptj1, ptj2) * deltaR_j1j2
			/// 4.i  if kt < ktcut, repeat from step 2 for harder branch 
			/// 4.ii if kt > ktcut, increment N_Lund by 1 and repeat from 
			///      step 2 for both branches j1 and j2 
			int count_lund_multiplicity(const PseudoJet& jet, const double& trackfrac=1, const double& ktcut = 1){
				// The Lund multiplicity algorithm 
				PseudoJet jj, j1, j2;
				int lund_mult = 1;

				jj = jet; // This is okay since only 1 jet was found

				// Go back through the declustering
				while (jj.has_parents(j1,j2)) {
					// make sure j1 is always harder branch
					if (j1.pt2() < j2.pt2()) std::swap(j1,j2);

					// collect info and compute the relative kt
					double delta_R = j1.delta_R(j2);
					double kt_rel = delta_R * j2.pt();

					// compare relative declustering kt and ktcut
					if( (kt_rel*trackfrac) > ktcut){
						// If the emission passes the kt_cut, recurse
						lund_mult += count_lund_multiplicity(j2, ktcut);
					}


					// Whatever happens, we continue following the
					// harder of the two branches
					jj = j1;
				}

				return lund_mult;
			}

			/// \brief Function that returns the Cambridge multiplicity of a clustered jet `jet'
			/// \param jet Pseudojet for which Cambridge mult is counted
			/// \param ktcut the ktcut value used for the counting
			///
			/// This function essentially reclusters the passed-in jet
			/// with Cambridge/Aachen and then counts it's Cambridge multiplicity
			/// with count_cambridge_multiplicity()
			int cambridge_multiplicity(const PseudoJet& jet, const double& trackfrac = 1, const double& ktcut = 1){
				// Recluster the jet with C/A, R=1
				// atm use R=2 just in case
				JetDefinition CA_jet_def = JetDefinition(fastjet::cambridge_algorithm, 2);
				ClusterSequence ca_cs(jet.constituents(), CA_jet_def);

				// Simple assert to see that things have gone right.
				// We expect only one jet to be found
				assert(ca_cs.inclusive_jets().size() == 1 && "Flawed event for dijet Cambridge multiplicity");    

				// Now we can count with the se of count_cambridge_multiplicity
				return count_cambridge_multiplicity(ca_cs.inclusive_jets()[0], trackfrac, ktcut);
			}

			/// \brief Function that resursively counts cambridge multiplicity
			/// \param jet Pseudojet for which Cambridge mult is counted
			/// \param ktcut the ktcut value used for the counting
			///
			/// The actual muscle of the function cambridge_multiplicity().
			///
			/// This function implements the Cambridge multiplicity counting
			/// as described in arXiv:2205.02861:
			/// 1.   set the N_Cambridge = 1
			/// 2.   undo one clustering step j -> j1 + j2 (where ptj1 > ptj2)
			/// 3.   determine kt = min(ptj1, ptj2) * deltaR_j1j2
			/// 4.i  if kt < ktcut, continue down main branch
			/// 4.ii if kt > ktcut, increment N_Cambridge by 1 and continue down main branch
			int count_cambridge_multiplicity(const PseudoJet& jet, const double& trackfrac=1, const double& ktcut = 1){
				// The Cambridge multiplicity algorithm 
				PseudoJet jj, j1, j2;
				int cambridge_mult = 1;

				jj = jet; // This is okay since only 1 jet was found

				// Go back through the declustering
				while (jj.has_parents(j1,j2)) {
					// make sure j1 is always harder branch
					if (j1.pt2() < j2.pt2()) std::swap(j1,j2);

					// collect info and compute the relative kt
					double delta_R = j1.delta_R(j2);
					double kt_rel = delta_R * j2.pt();

					// compare relative declustering kt and ktcut
					if( (kt_rel*trackfrac) > ktcut){
						// If the emission passes the kt_cut, increment by one
						cambridge_mult += 1;
					}

					// Whatever happens, we continue following the
					// harder of the two branches
					jj = j1;
				}

				return cambridge_mult;
			}


			// Given a list of bins, gives the bin which the value falls in
			// If it is below or above the last bin value, it will return -1
			int getBin(const double value, const std::vector<double> bins){
				for(unsigned int i=0; i<bins.size()-1; i++){
					if(value >= bins[i] && value < bins[i+1]) return i;
				}
				return -1;
			}


			// Currently, we are classifying pairs of hadrons into 8 types:
			// 0: both baryons
			// 1: baryon-charged meson
			// 2: baryon-neutral meson
			// 3: SS charged-meson charged-meson
			// 4: OS charged-meson charged-meson
			// 5: charged-meson neutral-meson
			// 6: neutral-meson neutral-meson
			// 7: charged-antimeson neutral meson
			// 8: baryon-antibaryon
			std::vector<int> getHadronPairType(const Particle& particle1, const Particle& particle2){
				// return array of which correlations get a fill
				std::vector<int> res;
				if(particle1.isBaryon() && particle2.isBaryon()){
					if (particle1.pid()*particle2.pid()>0) {
						res.push_back(0);
					}
					else {
						res.push_back(8);
					}
				}
				if(particle1.isBaryon() && particle2.isMeson()) {
					if(particle2.charge()) {
						res.push_back(1);
					}
					else {
						res.push_back(2);
					}
				}
				if(particle2.isBaryon() && particle1.isMeson()){
					if(particle1.charge()) {
						res.push_back(1);
					}
					else {
						res.push_back(2);
					}
				}

				if(particle1.isMeson() && particle2.isMeson()){
					int pid1 = particle1.pid();
					int pid2 = particle2.pid();
					// Both mesons neutral
					if(!particle1.charge() && !particle2.charge()) {
						res.push_back(6);
					}
					// Either meson neutral
					else if(!particle1.charge() || !particle2.charge()) {
						if((pid1 >0 && pid2>0) ||(pid1<0 && pid2<0)){
							res.push_back(7);
						}
						else {
							res.push_back(5);
						}
					}
					// Same-sign
					else if(particle1.charge() == particle2.charge()) {
						res.push_back(3); // Empty?
					}
					else {
						res.push_back(4);
					}
				}
				return res;
			}

			std::vector<std::pair<double, double> > getEECsNPoint(fastjet::PseudoJet jet_in, double energySum, int N, double jetRadius){
				std::vector<std::pair<double, double> > eecPairs;
				std::vector<std::vector<int> > indices;

				if(!jet_in.has_constituents()) return eecPairs;
				std::vector<fastjet::PseudoJet> constits = jet_in.constituents();

				std::vector<std::vector<int> > indexList = getPermutations(constits, N, indices);
				for(unsigned int i=0; i<indexList.size(); i++){
					std::pair<double, double>  eecPair = getEECNPoint(constits, energySum, indexList[i], N, jet_in, jetRadius);
					eecPairs.push_back(eecPair);
				}

				return eecPairs;
			}

			std::pair<double, double> getEECNPoint(std::vector<fastjet::PseudoJet> constits, double energySum, std::vector<int> indices, int N, fastjet::PseudoJet jet, double jetRadius){
				double pi=1.;
				double xLMax = 0;

				// Some safeguards against very low energies
				if(energySum<1e-10)  return std::make_pair(-20,-20);

				for(unsigned int i=0; i<indices.size(); i++){
					if(constits[indices[i]].e() < 1e-20) {
						return std::make_pair(-20,-20);
					}
					pi = pi*constits[indices[i]].e();
					for(unsigned int j=i+1; j<indices.size(); j++){
						if(constits[indices[j]].e() < 1e-20) continue;
						double xLij = constits[indices[i]].delta_R(constits[indices[j]]);

						if(xLij > xLMax){
							xLMax = xLij;
						}
					}
				}

				if(xLMax < 1e-20) return std::make_pair(-20,-20);

				return std::make_pair(pi / pow(energySum, N), log10(xLMax / jetRadius)); // This might still be wrong

			}

			std::vector<std::vector<int> > getPermutations(std::vector<fastjet::PseudoJet> constits, unsigned int N, std::vector<std::vector<int> > indices){
				std::vector<std::vector<int> > newIndices;
				if(constits.size()<N) return newIndices;

				if(indices.size() == 0) {
					for(unsigned int i=0; i<constits.size(); i++){
						std::vector<int> tmpInd;
						tmpInd.push_back(i);
						newIndices.push_back(tmpInd);
					}
					return getPermutations(constits, N, newIndices);
				}

				for(unsigned int i=0; i<indices.size(); i++){
					if(indices[i].size() >= N) {
						return indices;
					}

					if(indices[i].size() == 0) return indices;
					int nconstit = constits.size();
					int tmpInd = indices[i][0];
					for(int j = tmpInd+1; j<nconstit; j++){
						std::vector<int> tmpInd;
						tmpInd.push_back(j);
						for(unsigned int k=0; k<indices[i].size(); k++){
							tmpInd.push_back(indices[i][k]);
						}
						newIndices.push_back(tmpInd);
					}

				}

				return getPermutations(constits, N, newIndices);

			}

			std::vector<int> getParticleTypeBins(const Particle& particle){

				std::vector<int> particleType;
				particleType.push_back(0); // Just for validation of some things
				if(particle.isBaryon()) particleType.push_back(1);
				if(particle.isMeson()) particleType.push_back(2);
				if(particle.isMeson() && particle.charge()) particleType.push_back(3);
				if(particle.isMeson() && !particle.charge()) particleType.push_back(4);
				return particleType;
			}


			//////////////////////////////////////////////////////////////////////////////////////////////////

			/// @name Analysis methods
			/// @{

			/// Book histograms and initialise projections before the run
			void init() {
				// Initialise and register projections

				// The basic final-state projection:
				// all final-state particles within
				// the given eta acceptance
				const FinalState fs(Cuts::abseta < 4.9);

				// Basic jet kinematics
				Histo1DPtr tmp; 
				book(_h_leadJetPt, "leadJet_pT", 500, 0, 2500);
				book(_h_leadJetEta, "leadJet_eta", 50, -5, 5);
				book(_h_jetPt, "jet_pt", 500, 0, 2500);
				book(_h_jetEta, "jet_eta", 50, -5, 5);
				// The final-state particles declared above are clustered using FastJet with
				// the anti-kT algorithm and a jet-radius parameter jetRadius
				// muons and neutrinos are excluded from the clustering
				FastJets jet4(fs, FastJets::ANTIKT, m_jetRadius, JetAlg::Muons::NONE, JetAlg::Invisibles::NONE);
				declare(jet4, "Jets");

				ChargedFinalState tracks(Cuts::pT > 0.5*GeV && Cuts::abseta < m_maxEtaTracks);
				declare(tracks, "tracks");

				////////////////////////////////////////////////////////////////////////////////////////////////
				// Book histograms
				for(unsigned int idx_pt_bin=0; idx_pt_bin<m_bins_pt.size()-1; idx_pt_bin++)  {
					Profile1DPtr tmpProf1;
					_h_pt_nLund_profile.push_back(tmpProf1);
					book(_h_pt_nLund_profile[idx_pt_bin],  "nLund_pt" + to_string(int(m_bins_pt[idx_pt_bin]))+"_" + to_string(int(m_bins_pt[idx_pt_bin+1])) + "_profile",  m_bins_ktcut);

					Profile1DPtr tmpProf2;
					_h_pt_nCambridge_profile.push_back(tmpProf2);
					book(_h_pt_nCambridge_profile[idx_pt_bin],  "nCambridge_pt" + to_string(int(m_bins_pt[idx_pt_bin]))+"_" + to_string(int(m_bins_pt[idx_pt_bin+1])) + "_profile",  m_bins_ktcut);

					std::vector<Histo1DPtr> tmpVec1;
					std::vector<Histo1DPtr> tmpVec2;
					_h_pt_nLundKt.push_back(tmpVec1);
					_h_pt_nCambridgeKt.push_back(tmpVec2);
					for(unsigned int idx_kt_bin=0; idx_kt_bin<m_bins_ktcut.size()-1; idx_kt_bin++) {
						std::string histo_name_lund  = "nLund_KtCut_" + m_bins_ktcut_str[idx_kt_bin] + "_pt"+to_string(int(m_bins_pt[idx_pt_bin]))+"_" + to_string(int(m_bins_pt[idx_pt_bin+1])); 
						Histo1DPtr tmp; 
						_h_pt_nLundKt[idx_pt_bin].push_back(book(tmp, histo_name_lund,  60, 0, 60) );

						// Cambridge
						std::string histo_name_cambridge = "nCambridge_KtCut_" + m_bins_ktcut_str[idx_kt_bin]+"_pt"+to_string(int(m_bins_pt[idx_pt_bin]))+"_" + to_string(int(m_bins_pt[idx_pt_bin+1]));
						Histo1DPtr tmp2; 
						_h_pt_nCambridgeKt[idx_pt_bin].push_back(book(tmp2, histo_name_cambridge,  60, 0, 60) );
					}

					Histo1DPtr tmp; 
					_h_pt_angleCorrelations.push_back(tmp);
					std::string histName = "angleCorrelations_pT_" + to_string(int(m_bins_pt[idx_pt_bin]))+"_" + to_string(int(m_bins_pt[idx_pt_bin+1]));
					book(_h_pt_angleCorrelations[idx_pt_bin], histName, logspace(40, 0.001, 2*m_jetRadius));

					std::vector<Histo1DPtr> tmpVecHadType;
					_h_pt_angleCorrelations_byHadronType.push_back(tmpVecHadType);
					for(unsigned int idx_had_bin=0; idx_had_bin<m_nHadronPairTypes; idx_had_bin++){
						_h_pt_angleCorrelations_byHadronType[idx_pt_bin].push_back(tmp);
						std::string histName = "angleCorrelations_pT_" + to_string(int(m_bins_pt[idx_pt_bin])) +"_" + to_string(int(m_bins_pt[idx_pt_bin+1])) + "_hadronType_" + m_hadronPairStrings[idx_had_bin];
						book(_h_pt_angleCorrelations_byHadronType[idx_pt_bin][idx_had_bin], histName, logspace(40, 0.001, 2*m_jetRadius));
					}


					Histo1DPtr tmpDpsi;
					Histo2DPtr tmpLJP_z;
					Histo2DPtr tmpLJP_kt;
					_h_pt_deltaPsi.push_back(tmpDpsi);
					_h_pt_lundJetPlane_z.push_back(tmpLJP_z);
					_h_pt_lundJetPlane_kt.push_back(tmpLJP_kt);
					book(_h_pt_deltaPsi[idx_pt_bin], "deltaPsi12_pT_" + to_string(int(m_bins_pt[idx_pt_bin]))+"_" + to_string(int(m_bins_pt[idx_pt_bin+1])), 50, 0, 6.28);
					book(_h_pt_lundJetPlane_z[idx_pt_bin], "lundJetPlane_z_pT_" + to_string(int(m_bins_pt[idx_pt_bin]))+"_" + to_string(int(m_bins_pt[idx_pt_bin+1])), 50, 0, 5, 50, 0, 6);
					book(_h_pt_lundJetPlane_kt[idx_pt_bin], "lundJetPlane_kt_pT_" + to_string(int(m_bins_pt[idx_pt_bin]))+"_" + to_string(int(m_bins_pt[idx_pt_bin+1])), 50, 0, 5, 50, -3, 6);


					std::vector<Histo1DPtr> tmpVecHadFrac;
					std::vector<Histo1DPtr> tmpVecHadN;
					_h_pt_energyFraction_particleType.push_back(tmpVecHadFrac);
					_h_pt_nParticles_particleType.push_back(tmpVecHadN);
					for(unsigned int idx_part_bin=0; idx_part_bin<m_nParticleTypes; idx_part_bin++){
						Histo1DPtr tmp1; 
						_h_pt_energyFraction_particleType[idx_pt_bin].push_back(tmp1);
						std::string histName = "energyFraction_pT_" + to_string(int(m_bins_pt[idx_pt_bin]))+"_" + to_string(int(m_bins_pt[idx_pt_bin+1])) + "_particleType_" + m_particleTypeNames[idx_part_bin];
						book(_h_pt_energyFraction_particleType[idx_pt_bin][idx_part_bin], histName, 50, 0, 1);

						Histo1DPtr tmp2; 
						_h_pt_nParticles_particleType[idx_pt_bin].push_back(tmp2);
						std::string histNameNPart = "nParticles_pT_" + to_string(int(m_bins_pt[idx_pt_bin]))+"_" + to_string(int(m_bins_pt[idx_pt_bin+1])) + "_particleType_" + m_particleTypeNames[idx_part_bin];
						book(_h_pt_nParticles_particleType[idx_pt_bin][idx_part_bin], histNameNPart, 50, 0, 50);
					}


					std::string histo_nameEEC2  = "EEC2p_pt_" + to_string(int(m_bins_pt[idx_pt_bin]))+"_" + to_string(int(m_bins_pt[idx_pt_bin+1]));
					Histo1DPtr tmpEEC2;
					_h_pt_eec2p.push_back(book(tmpEEC2, histo_nameEEC2,  45, -3.5, 1) );

					std::string histo_nameEEC3  = "EEC3p_pt_" + to_string(int(m_bins_pt[idx_pt_bin]))+"_" + to_string(int(m_bins_pt[idx_pt_bin+1]));
					Histo1DPtr tmpEEC3;
					_h_pt_eec3p.push_back(book(tmpEEC3, histo_nameEEC3,  45, -3.5, 1) );

					std::string histo_nameEEC4  = "EEC4p_pt_" + to_string(int(m_bins_pt[idx_pt_bin]))+"_" + to_string(int(m_bins_pt[idx_pt_bin+1]));
					Histo1DPtr tmpEEC4;
					_h_pt_eec4p.push_back(book(tmpEEC4, histo_nameEEC4,  45, -3.5, 1) );

                                        std::string histo_nameEEC2_normPerJet  = "EEC2p_pt_normPerJet_" + to_string(int(m_bins_pt[idx_pt_bin]))+"_" + to_string(int(m_bins_pt[idx_pt_bin+1]));
                                        Histo1DPtr tmpEEC2_normPerJet;
                                        _h_pt_eec2p_normPerJet.push_back(book(tmpEEC2_normPerJet, histo_nameEEC2_normPerJet,  45, -3.5, 1) );

                                        std::string histo_nameEEC3_normPerJet  = "EEC3p_pt_normPerJet_" + to_string(int(m_bins_pt[idx_pt_bin]))+"_" + to_string(int(m_bins_pt[idx_pt_bin+1]));
                                        Histo1DPtr tmpEEC3_normPerJet;
                                        _h_pt_eec3p_normPerJet.push_back(book(tmpEEC3_normPerJet, histo_nameEEC3_normPerJet,  45, -3.5, 1) );

                                        std::string histo_nameEEC4_normPerJet  = "EEC4p_pt_normPerJet_" + to_string(int(m_bins_pt[idx_pt_bin]))+"_" + to_string(int(m_bins_pt[idx_pt_bin+1]));
                                        Histo1DPtr tmpEEC4_normPerJet;
                                        _h_pt_eec4p_normPerJet.push_back(book(tmpEEC4_normPerJet, histo_nameEEC4_normPerJet,  45, -3.5, 1) );


				}

				for(unsigned int idx_part_bin=0; idx_part_bin<m_nParticleTypes; idx_part_bin++){
					Profile1DPtr tmpProfHadFrac;
					_h_pt_energyFraction_profile.push_back(tmpProfHadFrac);
					book(_h_pt_energyFraction_profile[idx_part_bin],  "energyFraction_particleType" + m_particleTypeNames[idx_part_bin] + "_profile",  m_bins_pt);
					Profile1DPtr tmpProfHadN;
					_h_pt_nParticles_profile.push_back(tmpProfHadN);
					book(_h_pt_nParticles_profile[idx_part_bin],  "nParticles_particleType" + m_particleTypeNames[idx_part_bin] + "_profile",  m_bins_pt);

				}

				// njets, for normalisation
				book(_njets, "_njets", m_bins_pt);
			}


			/// Perform the per-event analysis
			void analyze(const Event& event) {

				// Retrieve clustered jets, sorted by pT, with a minimum pT cut
				const Jets jets = apply<JetAlg>(event, "Jets").jetsByPt(Cuts::pT > m_minJetPt*GeV && Cuts::abseta < m_maxEta);
				if(m_debug)std::cout << jets.size() <<"\t" << std::endl;

				if (jets.size() < 2)  vetoEvent;                    // Dijet events
				if(m_debug)std::cout << jets[0].pt() << "\t" << jets[1].pt() << std::endl;
				if ( (jets[0].pT()/jets[1].pT()) > 1.5 ) vetoEvent; // Dijet events -- balance requirement

				const Particles& tracks = apply<ChargedFinalState>(event, "tracks").particlesByPt();

				// For now, only selecting the leading two jets
				std::vector<Jet> selectedJets;
				selectedJets.push_back(jets[0]);
				selectedJets.push_back(jets[1]);

				_h_leadJetPt->fill(jets[0].pt());
				_h_leadJetPt->fill(jets[0].eta());

				for(unsigned int idx_jet=0; idx_jet < selectedJets.size(); idx_jet++){
					_h_jetPt->fill(selectedJets[idx_jet].pt());
					_h_jetEta->fill(selectedJets[idx_jet].eta());


					Particles intracks1;
					for (const Particle& p : tracks) {
						const double dr = deltaR(selectedJets[idx_jet], p, PSEUDORAPIDITY);
						if (dr > m_jetRadius) continue;
						if (abs(p.pid()) == 13) continue; // no muons
						intracks1.push_back(p);
					}

					// Recluster charged particles in jets ('tjets' --> 'track jets') with C/A algorithm
					JetDefinition tjet_def(fastjet::cambridge_algorithm, fastjet::JetDefinition::max_allowable_R);
					ClusterSequence tjet1_cs(intracks1, tjet_def);
					vector<PseudoJet> tjets1 = fastjet::sorted_by_pt(tjet1_cs.inclusive_jets(0.0));

					// Counting jets -- only count those passing the fiducial requirement of the jet pT
					if (selectedJets[idx_jet].pT() > m_bins_pt[0]*GeV) _njets->fill(selectedJets[idx_jet].pT());

					////////////////////////////////////////////////////////////////////////////////////////////////////////
					double trackfrac1 = 0;
					if(tjets1.size()>0){
						trackfrac1 = selectedJets[idx_jet].pt()/tjets1[0].perp(); // 1 / fraction of jet pT carried by charged particles
					}
					int pTbin = getBin(selectedJets[idx_jet].pt(), m_bins_pt);

					if(pTbin < 0) continue;
					for(unsigned int idx_kt_bin=0; idx_kt_bin<m_bins_ktcut.size()-1; idx_kt_bin++) {
						int n1LundKt = 0;
						int n1CambridgeKt = 0;
						if(tjets1.size()){
							n1LundKt = lund_multiplicity(tjets1[0], trackfrac1, m_bins_ktcut[idx_kt_bin]); 
							n1CambridgeKt = cambridge_multiplicity(tjets1[0], trackfrac1, m_bins_ktcut[idx_kt_bin]); 
						}
						assert(n1LundKt >= n1CambridgeKt && "WARNING\tCAMBRIDGE > LUND\tkt_cut=XXX GeV ");
						if(m_debug) std::cout << "Multipicities for kt = " << m_bins_ktcut[idx_kt_bin] << ": " << n1LundKt << "\t" << n1CambridgeKt << std::endl;
						_h_pt_nLundKt[pTbin][idx_kt_bin]->fill(n1LundKt);
						_h_pt_nCambridgeKt[pTbin][idx_kt_bin]->fill(n1CambridgeKt);

						_h_pt_nLund_profile[pTbin]->fill(m_bins_ktcut[idx_kt_bin],   n1LundKt);
						_h_pt_nCambridge_profile[pTbin]->fill(m_bins_ktcut[idx_kt_bin],   n1CambridgeKt);
					}


					std::vector<double> particleEnergy;
					std::vector<int> nparticles;
					for(unsigned int idx_npart_bin=0; idx_npart_bin <= m_nParticleTypes; idx_npart_bin++){
						particleEnergy.push_back(0);
						nparticles.push_back(0);
					}
					for(unsigned int iparticle1=0; iparticle1 < selectedJets[idx_jet].constituents().size(); iparticle1++){
						Particle particle1 = selectedJets[idx_jet].constituents()[iparticle1];
						std::vector<int> nPartBins = getParticleTypeBins(particle1);
						for(unsigned int i=0; i<nPartBins.size(); i++){
							particleEnergy[nPartBins[i]] += particle1.E();
							nparticles[nPartBins[i]] += 1;
						}

						for(unsigned int iparticle2=iparticle1+1; iparticle2 < selectedJets[idx_jet].constituents().size(); iparticle2++){
							Particle particle2 = selectedJets[idx_jet].constituents()[iparticle2];
							double dR = getCorrelation(particle1, particle2);
							_h_pt_angleCorrelations[pTbin]->fill(dR);
							std::vector<int> hadronBins = getHadronPairType(particle1, particle2);
							if(hadronBins.size() == 0) continue;
							if(hadronBins.size() != 1) {
								// will never print, but keep a note for more complicated correlations
								std::cout << "Note: Nfills for angleCorrelations = "<< hadronBins.size()<<"\nList:";
								for (auto i : hadronBins) {
									std::cout << i << std::endl;
								}
							}
							for (auto hadronBin: hadronBins){
								_h_pt_angleCorrelations_byHadronType[pTbin][hadronBin]->fill(dR);
							}
						}
					}

					for(unsigned int idx_npart_bin=0; idx_npart_bin < m_nParticleTypes; idx_npart_bin++){
						_h_pt_energyFraction_particleType[pTbin][idx_npart_bin]->fill(particleEnergy[idx_npart_bin] / selectedJets[idx_jet].E());
						_h_pt_nParticles_particleType[pTbin][idx_npart_bin]->fill(nparticles[idx_npart_bin] );
						_h_pt_energyFraction_profile[idx_npart_bin]->fill(selectedJets[idx_jet].pt(), particleEnergy[idx_npart_bin] / selectedJets[idx_jet].E());
						_h_pt_nParticles_profile[idx_npart_bin]->fill(selectedJets[idx_jet].pt(), nparticles[idx_npart_bin]);
					}

					std::vector<Declustering> clusts = jet_declusterings(selectedJets[idx_jet]);

					double dPsi =  deltaPsi(clusts);
					_h_pt_deltaPsi[pTbin]->fill(dPsi);
					for(unsigned int idx_clust=0; idx_clust < clusts.size(); idx_clust++){
						if(m_debug) std::cout << clusts[idx_clust].delta_R  << "\t" << clusts[idx_clust].z << "\t" << clusts[idx_clust].kt << "\t" << log(m_jetRadius/clusts[idx_clust].delta_R ) <<"\t" <<  log(1./clusts[idx_clust].z) << "\t" << log(clusts[idx_clust].kt) << std::endl;
						_h_pt_lundJetPlane_z[pTbin]->fill(log(m_jetRadius/clusts[idx_clust].delta_R ), log(1./clusts[idx_clust].z));
						_h_pt_lundJetPlane_kt[pTbin]->fill(log(m_jetRadius/clusts[idx_clust].delta_R ), log(clusts[idx_clust].kt));
					}



					double trackEnergy = 0;
					if (tjets1.size()>0) {
						for(unsigned int i=0; i<tjets1[0].constituents().size(); i++){
							trackEnergy += tjets1[0].constituents()[i].e();
						}

						std::vector<std::pair<double, double> > eecs2p = getEECsNPoint(tjets1[0], trackEnergy, 2, m_jetRadius);
						std::vector<std::pair<double, double> > eecs3p = getEECsNPoint(tjets1[0], trackEnergy, 3, m_jetRadius);
						std::vector<std::pair<double, double> > eecs4p = getEECsNPoint(tjets1[0], trackEnergy, 4, m_jetRadius);

						for(unsigned int ipair=0; ipair < eecs2p.size(); ipair++){
							_h_pt_eec2p[pTbin]->fill(eecs2p[ipair].second, eecs2p[ipair].first);
							_h_pt_eec2p_normPerJet[pTbin]->fill(eecs2p[ipair].second, eecs2p[ipair].first);
						}
						for(unsigned int ipair=0; ipair < eecs3p.size(); ipair++){
							_h_pt_eec3p[pTbin]->fill(eecs3p[ipair].second, eecs3p[ipair].first);
							_h_pt_eec3p_normPerJet[pTbin]->fill(eecs3p[ipair].second, eecs3p[ipair].first);
						}
						for(unsigned int ipair=0; ipair < eecs4p.size(); ipair++){
							_h_pt_eec4p[pTbin]->fill(eecs4p[ipair].second, eecs4p[ipair].first);
							_h_pt_eec4p_normPerJet[pTbin]->fill(eecs4p[ipair].second, eecs4p[ipair].first);
						}
					}




				} // end loop over jets

			}

			/// Normalise histograms etc., after the run
			void finalize() 
			{      
				/// make everything an xsec first
				double scl = crossSection()/picobarn/sumOfWeights();

				for(auto hvec: {
						_h_pt_nLundKt, 
						_h_pt_nCambridgeKt,
						_h_pt_angleCorrelations_byHadronType,
						_h_pt_energyFraction_particleType,
						_h_pt_nParticles_particleType,
						{
						_h_pt_angleCorrelations,
						_h_pt_deltaPsi,
						_h_pt_eec2p,
						_h_pt_eec3p,
						_h_pt_eec4p,
						_h_pt_eec2p_normPerJet,
						_h_pt_eec3p_normPerJet,
						_h_pt_eec4p_normPerJet,
						{_h_leadJetPt, _njets, _h_leadJetEta, _h_jetPt, _h_jetEta}
						}
						}) {
					for(auto h: hvec) scale(h,scl);
				}

				for(auto hvec: {_h_pt_lundJetPlane_z,
						_h_pt_lundJetPlane_kt}) {
					for(auto h: hvec) scale(h,scl);
				}
				// Normalise by the number of jets ...
				for(unsigned int idx_pt_bin=0; idx_pt_bin<m_bins_pt.size()-1; idx_pt_bin++) {
					double njets = _njets->integralRange(idx_pt_bin, idx_pt_bin);
					if(njets==0) continue;
					std::cout << "Number of jets for " << m_bins_pt[idx_pt_bin] << ": " << njets << "\t" << _h_pt_nLundKt[idx_pt_bin][0]->integral() << std::endl;
					normalize( _h_pt_angleCorrelations[idx_pt_bin], njets);
					for(unsigned int idx_had_bin=0; idx_had_bin<m_nHadronPairTypes; idx_had_bin++) {
						normalize( _h_pt_angleCorrelations_byHadronType[idx_pt_bin][idx_had_bin], njets);
					}
					for(unsigned int idx_kt_bin=0; idx_kt_bin<m_bins_ktcut.size()-1; idx_kt_bin++) {
						normalize(_h_pt_nLundKt[idx_pt_bin][idx_kt_bin], njets);
						normalize(_h_pt_nCambridgeKt[idx_pt_bin][idx_kt_bin], njets);
					}
					for(unsigned int idx_part_bin=0; idx_part_bin<m_nParticleTypes; idx_part_bin++) {
						normalize(_h_pt_energyFraction_particleType[idx_pt_bin][idx_part_bin], njets);
						normalize(_h_pt_nParticles_particleType[idx_pt_bin][idx_part_bin], njets);
					}
					normalize(_h_pt_deltaPsi[idx_pt_bin], njets);
					normalize(_h_pt_lundJetPlane_z[idx_pt_bin], njets);
					normalize(_h_pt_lundJetPlane_kt[idx_pt_bin], njets);

					normalize( _h_pt_eec2p[idx_pt_bin]);
					normalize( _h_pt_eec3p[idx_pt_bin]);
					normalize( _h_pt_eec4p[idx_pt_bin]);

					normalize( _h_pt_eec2p_normPerJet[idx_pt_bin], njets);
					normalize( _h_pt_eec3p_normPerJet[idx_pt_bin], njets);
					normalize( _h_pt_eec4p_normPerJet[idx_pt_bin], njets);
				}
			}

			bool m_debug = false;
			const vector<double> m_bins_pt = {30, 60, 100, 250., 500., 1000., 9999.}; // last bin is 1250+ inclusive


			const double m_maxEtaTracks = 2.5;
			const double m_maxEta = 2.1;
			//const double m_maxEta = 5.0;
			const double m_minJetPt = 5;
			const double m_jetRadius = 0.4;

			///////////////////////////////////////////////////////////
			// Cambridge and Lund multiplicities
			///////////////////////////////////////////////////////////

			const vector<double> m_bins_ktcut = {0.5, 1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0, 150.0}; // last kt cut is 100 GeV
			const vector<std::string> m_bins_ktcut_str = {"0005", "0010", "0020", "0050", "0100", "0200", "0500", "1000", "2000"};
			std::vector<std::vector<Histo1DPtr> > _h_pt_nLundKt;
			std::vector<std::vector<Histo1DPtr> > _h_pt_nCambridgeKt;
			std::vector<Profile1DPtr> _h_pt_nLund_profile;
			std::vector<Profile1DPtr> _h_pt_nCambridge_profile;

			///////////////////////////////////////////////////////////
			// Correlations between particles
			///////////////////////////////////////////////////////////

			// Currently, we are classifying pairs of hadrons into 11 types:
			// 0: both baryons
			// 1: baryon-charged meson
			// 2: baryon-neutral meson
			// 3: SS charged-meson charged-meson
			// 4: OS charged-meson charged-meson
			// 5: charged-meson neutral-meson
			// 6: neutral-meson neutral-meson
			// 7: charged-antimeson neutral meson
			// 8: baryon-antibaryon
			const std::vector<std::string> m_hadronPairStrings = {
				"baryon_baryon",
				"baryon_chargedMeson",
				"baryon_neutralMeson",
				"sameSign_meson_meson",
				"oppositeSign_meson_meson",
				"charged_neutral_meson",
				"allNeutralMeson",
				"chargedAntimeson_neutralMeson",
				"baryon_antibaryon"
			};
			const unsigned int m_nHadronPairTypes = m_hadronPairStrings.size();

			std::vector<std::vector<Histo1DPtr> > _h_pt_angleCorrelations_byHadronType;
			std::vector<Histo1DPtr> _h_pt_angleCorrelations;

			///////////////////////////////////////////////////////////
			// Energy fractions and particle counts for different particle types
			///////////////////////////////////////////////////////////

			const unsigned int m_nParticleTypes = 5;
			const std::vector<std::string> m_particleTypeNames = {"All_particles", "Baryons", "Mesons", "Charged_mesons", "Neutral_mesons"};
			std::vector<std::vector<Histo1DPtr> > _h_pt_energyFraction_particleType;
			std::vector<std::vector<Histo1DPtr> > _h_pt_nParticles_particleType;
			std::vector<Profile1DPtr> _h_pt_energyFraction_profile;
			std::vector<Profile1DPtr> _h_pt_nParticles_profile;

			std::vector<Histo1DPtr> _h_pt_deltaPsi;
			std::vector<Histo2DPtr> _h_pt_lundJetPlane_z;
			std::vector<Histo2DPtr> _h_pt_lundJetPlane_kt;


			///////////////////////////////////////////////////////////
			// Energy-energy correlators (2,3,4-point)
			// Note that these can be fairly slow to compute,
			// so we may want to remove the 4-point correlator
			///////////////////////////////////////////////////////////

			std::vector<Histo1DPtr> _h_pt_eec2p;
			std::vector<Histo1DPtr> _h_pt_eec3p;
			std::vector<Histo1DPtr> _h_pt_eec4p;

			std::vector<Histo1DPtr> _h_pt_eec2p_normPerJet;
			std::vector<Histo1DPtr> _h_pt_eec3p_normPerJet;
			std::vector<Histo1DPtr> _h_pt_eec4p_normPerJet;

			///////////////////////////////////////////////////////////
			// Used for normalizations
			///////////////////////////////////////////////////////////
			Histo1DPtr _njets;

			// Basic jet kinematics
			Histo1DPtr _h_leadJetPt;
			Histo1DPtr _h_leadJetEta;
			Histo1DPtr _h_jetPt;
			Histo1DPtr _h_jetEta;


	};


	RIVET_DECLARE_PLUGIN(MC_LESHOUCHES23);

	}
