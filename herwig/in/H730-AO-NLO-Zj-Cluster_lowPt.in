# -*- ThePEG-repository -*-

##################################################
## Herwig/Matchbox example input file
##################################################

##################################################
## Collider type
##################################################
read snippets/Matchbox.in
read snippets/PPCollider.in

##################################################
## Beam energy sqrt(s)
##################################################

cd /Herwig/EventHandlers
set EventHandler:LuminosityFunction:Energy 13000*GeV

##################################################
## Process selection
##################################################

## Note that event generation may fail if no matching matrix element has
## been found.  Coupling orders are with respect to the Born process,
## i.e. NLO QCD does not require an additional power of alphas.

## Model assumptions
read Matchbox/StandardModelLike.in
read Matchbox/DiagonalCKM.in

## Set the order of the couplings
cd /Herwig/MatrixElements/Matchbox
set Factory:OrderInAlphaS 1
set Factory:OrderInAlphaEW 2


set /Herwig/EventHandlers/EventHandler:Weighted Yes
## Select the process
## You may use identifiers such as p, pbar, j, l, mu+, h0 etc.
do Factory:Process p p -> l+ l- j
## adding a pT^4 Preweighter for better stats
cd /Herwig/MatrixElements/Matchbox
create Herwig::MergingReweight MPreWeight HwDipoleShower.so
insert Factory:Preweighters 0  MPreWeight
set MPreWeight:HTPower 0
set MPreWeight:MaxPTPower 4
set MPreWeight:OnlyColoured No

## Special settings required for on-shell production of unstable particles
## enable for on-shell top production
# read Matchbox/OnShellTopProduction.in
## enable for on-shell W, Z or h production
# read Matchbox/OnShellWProduction.in
# read Matchbox/OnShellZProduction.in
# read Matchbox/OnShellHProduction.in
# Special settings for the VBF approximation
# read Matchbox/VBFDiagramsOnly.in

##################################################
## Matrix element library selection
##################################################

## Select a generic tree/loop combination or a
## specialized NLO package

# read Matchbox/MadGraph-GoSam.in
# read Matchbox/MadGraph-MadGraph.in
# read Matchbox/MadGraph-NJet.in
# read Matchbox/MadGraph-OpenLoops.in
read Matchbox/OpenLoops-OpenLoops.in
# read Matchbox/HJets.in
# read Matchbox/VBFNLO.in

## Uncomment this to use ggh effective couplings
## currently only supported by MadGraph-GoSam

# read Matchbox/HiggsEffective.in

##################################################
## Cut selection
## See the documentation for more options
##################################################
cd /Herwig/Cuts/
set ChargedLeptonPairMassCut:MinMass 60*GeV
set ChargedLeptonPairMassCut:MaxMass 120*GeV

## cuts on additional jets
read Matchbox/DefaultPPJets.in

insert JetCuts:JetRegions 0 FirstJet
set FirstJet:PtMin 30*GeV 
# set FirstJet:PtMin 500*GeV
do FirstJet:YRange -5.0 5.0
set FirstJet:Fuzzy No
##################################################
## Scale choice
## See the documentation for more options
##################################################

cd /Herwig/MatrixElements/Matchbox
set Factory:ScaleChoice /Herwig/MatrixElements/Matchbox/Scales/HTScale
set /Herwig/MatrixElements/Matchbox/Scales/HTScale:JetPtCut 15.0*GeV
##################################################
## Matching and shower selection
## Please also see flavour scheme settings
## towards the end of the input file.
##################################################

# read Matchbox/MCatNLO-DefaultShower.in
read Matchbox/Powheg-DefaultShower.in
## use for strict LO/NLO comparisons
# read Matchbox/MCatLO-DefaultShower.in
## use for improved LO showering
# read Matchbox/LO-DefaultShower.in

# read Matchbox/MCatNLO-DipoleShower.in
# read Matchbox/Powheg-DipoleShower.in
## use for strict LO/NLO comparisons
# read Matchbox/MCatLO-DipoleShower.in
## use for improved LO showering
# read Matchbox/LO-DipoleShower.in

# read Matchbox/NLO-NoShower.in
# read Matchbox/LO-NoShower.in

##################################################
## Scale uncertainties
##################################################

# read Matchbox/MuDown.in
# read Matchbox/MuUp.in

##################################################
## Shower scale uncertainties
##################################################

# read Matchbox/MuQDown.in
# read Matchbox/MuQUp.in

##################################################
## PDF choice
##################################################

read Matchbox/FiveFlavourScheme.in
## required for dipole shower and fixed order in five flavour scheme
# read Matchbox/FiveFlavourNoBMassScheme.in
read Matchbox/CT14.in
# read Matchbox/MMHT2014.in

##################################################
## Analyses
##################################################

# cd /Herwig/Analysis
# insert Rivet:Analyses 0 XXX_2015_ABC123
# insert /Herwig/Generators/EventGenerator:AnalysisHandlers 0 Rivet
# insert /Herwig/Generators/EventGenerator:AnalysisHandlers 0 HepMC

##################################################
## Save the generator
##################################################

do /Herwig/MatrixElements/Matchbox/Factory:ProductionMode
cd /Herwig/Generators
# read snippets/Rivet.in
insert EventGenerator:AnalysisHandlers 0 /Herwig/Analysis/Rivet
read in/LH2023_ana_Zj.in
# insert EventGenerator:AnalysisHandlers 0 /Herwig/Analysis/Rivet
# cd /Herwig/Analysis
# insert Rivet:Analyses 0 MC_LESHOUCHES23
# insert Rivet:Analyses 0 ATLAS_2020_I1790256
# insert Rivet:Analyses 0 ATLAS_2020_I1808726
# insert Rivet:Analyses 0 ATLAS_2019_I1772062
# insert Rivet:Analyses 0 ATLAS_2019_I1724098:MODE=DJ
# insert Rivet:Analyses 0 ATLAS_2019_I1740909
# insert Rivet:Analyses 0 ATLAS_2018_I1634970
# insert Rivet:Analyses 0 ATLAS_2021_I1913061
# insert Rivet:Analyses 0 ATLAS_2018_I1711114
# insert Rivet:Analyses 0 CMS_2021_I1920187:MODE=DIJET
# insert Rivet:Analyses 0 CMS_2018_I1682495
# insert Rivet:Analyses 0 CMS_2021_I1972986


cd /Herwig/Generators
insert EventGenerator:AnalysisHandlers 0 /Herwig/Analysis/HepMCFile
set /Herwig/Analysis/HepMCFile:PrintEvent 10000
set /Herwig/Analysis/HepMCFile:Format GenEvent
set /Herwig/Analysis/HepMCFile:Units GeV_mm
set /Herwig/Analysis/HepMCFile:Filename hepMC/H730-AO-NLO-Zj-Cluster_lowPt.hepmc
##################################################
# Save run for later usage with 'Herwig run'
##################################################
cd /Herwig/Generators
saverun run/H730-AO-NLO-Zj-Cluster_lowPt EventGenerator
